//
//  ExperienceTableViewController.swift
//  CVApp
//
//  Created by William Segelström on 2019-11-01.
//  Copyright © 2019 William Segelström. All rights reserved.
//

import UIKit

class ExperienceTableViewController: UITableViewController {
    //MARK: - Attribute & Row types
    struct timeSpan {
        let fromDate: String
        let toDate: String
    }
    
    struct experienceRow {
        var title:String?
        let place:String
        let during: timeSpan
        var icon:UIImage?
        let description:String
    }
    //MARK: - Table data
    let experienceRows: [[experienceRow]] = [[
        experienceRow(title: "Teacher", place: "KomTek", during: timeSpan(fromDate: "2015-11-02", toDate: "2016-01-31"), icon: UIImage.init(systemName: "pencil"), description: "Technical!"),
        experienceRow(title: "Cashier", place: "ICA", during: timeSpan(fromDate:"2017-05-01", toDate: "2018-09-09"), description: "Not so technical")
    ],[
        experienceRow(title: nil, place: "Tullängsgymnasiet", during: timeSpan(fromDate: "2014-09-09", toDate: "2017-04-01"), icon: UIImage.init(named: "EduTull"), description: "Very technical!"),
        experienceRow(title: nil, place: "Jönköping University", during: timeSpan(fromDate: "2018-09-09", toDate: "2021-01-01"), icon: UIImage.init(named: "EduJu"), description: "Even more technical!")
    ]]
    //Sections
    let experienceSections: [String] = [
        "Work",
        "Education"
    ]
    //MARK: - Logic
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return experienceSections.count
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return experienceSections[section]
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return experienceRows[section].count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "experienceCell", for: indexPath)
        let cellAttributes:experienceRow = experienceRows[indexPath.section][indexPath.item]
        
        var headerTitle:String
        let detailText:String = cellAttributes.during.fromDate + " til " + cellAttributes.during.toDate
        
        if cellAttributes.title != nil {
            headerTitle = cellAttributes.title! + " at " + cellAttributes.place
        } else {
            headerTitle = cellAttributes.place
        }
        
        cell.textLabel?.text = headerTitle
        cell.detailTextLabel?.text = detailText
        cell.imageView?.image = cellAttributes.icon ?? UIImage.init(systemName: "xmark.circle")
        
        return cell
    }

    // MARK: - Navigation
    var dataToSend:experienceRow?
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dataToSend = experienceRows[indexPath.section][indexPath.item]
        performSegue(withIdentifier: "ViewDetailPage", sender: dataToSend)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationView = segue.destination as? ExperiencePageViewController
        let data = sender as? experienceRow
        let duringDateRange = data!.during.fromDate + " - " + data!.during.toDate
        
        destinationView?.setAttributesForDetail(title: data?.title ?? data?.place, detail: duringDateRange, icon: data?.icon, bodyContent: data?.description)
        
    }
    

}

//
//  ExperiencePageViewController.swift
//  CVApp
//
//  Created by William Segelström on 2019-11-02.
//  Copyright © 2019 William Segelström. All rights reserved.
//

import UIKit

class ExperiencePageViewController: UIViewController {
    var pageTitle:String?
    var pageDurationDetail:String?
    var pageIcon:UIImage?
    var pageDescription:String?
    
    func setAttributesForDetail(title:String?, detail:String?, icon:UIImage?, bodyContent:String?) {
        pageTitle = title
        pageDurationDetail = detail
        pageIcon = icon
        pageDescription = bodyContent
    }
    
    //MARK: Properties
    @IBOutlet weak var labelPageTitle: UILabel!
    @IBOutlet weak var labelPageDurationDetail: UILabel!
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var labelPageDescription: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        labelPageTitle.text = pageTitle
        labelPageDurationDetail.text = pageDurationDetail
        imageViewIcon.image = pageIcon
        labelPageDescription.text = pageDescription
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

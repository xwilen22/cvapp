//
//  SkillsModalViewController.swift
//  CVApp
//
//  Created by William Segelström on 2019-11-01.
//  Copyright © 2019 William Segelström. All rights reserved.
//

import UIKit

class SkillsModalViewController: UIViewController {
    @IBOutlet weak var backgroundWater: UIView!
    @IBOutlet weak var backgroundWaves: UIImageView!
    @IBOutlet weak var backgroundDarker: UIView!
    @IBOutlet weak var contentContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    //MARK: - Animation
    override func viewWillAppear(_ animated: Bool) {
        self.backgroundWater.center.y += self.view.bounds.height + self.backgroundWaves.bounds.height
        self.backgroundWaves.center.x += self.backgroundWaves.bounds.width / 2
        self.backgroundDarker.center.y += self.backgroundDarker.bounds.height
        
        self.contentContainer.alpha = CGFloat(0)
    }
    override func viewDidAppear(_ animated: Bool) {
        //Lighter part
        UIView.animate(withDuration: 0.75, delay: 0, options: .curveEaseIn, animations: {
            self.backgroundWater.center.y -= (self.view.bounds.height + self.backgroundWaves.bounds.height)
        })
        //Darker part
        UIView.animate(withDuration: 0.5, delay: 0.75, options: .curveEaseOut, animations: {
            self.backgroundDarker.center.y -= self.backgroundDarker.bounds.height
        }, completion: {(true) -> Void in
            UIView.animate(withDuration: 0.2, animations: {
                self.contentContainer.alpha = CGFloat(1)
            })
            
        })
        //Waveanimation
        UIView.animate(withDuration: 2, delay: 0, options: .curveLinear, animations: {
            self.backgroundWaves.center.x -= self.backgroundWaves.bounds.width / 2
        })
    }
   // MARK: - Navigation
    @IBAction func closeModal(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
